package com.example.demo.Service;

import com.example.demo.Model.ProductsEntity;
import com.example.demo.dto.ProductsRequest;
import com.example.demo.dto.ProductsResponse;

import java.util.List;

public interface ProductService {
    public ProductsResponse getProduct (Integer id);
    public List<ProductsResponse> getAllProduct ();

    public void deleteProduct(Integer id);
    public void saveProduct(ProductsRequest productsRequest);
    public void updateProduct(Integer id, ProductsRequest productsRequest);

}
