package com.example.demo.Service;

import com.example.demo.Convert.ConvertDTOtoEntity;
import com.example.demo.Convert.ConvertEntitytoDTO;
import com.example.demo.Model.ProductsEntity;
import com.example.demo.Repository.ProductsRepository;
import com.example.demo.dto.ProductsRequest;
import com.example.demo.dto.ProductsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.ResourceAccessException;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductsRepository productsRepository;
    @Override
    public ProductsResponse getProduct(Integer id)
    {

        ProductsEntity productsEntity = productsRepository.findById(id).get();
        ConvertEntitytoDTO convertEntitytoDTO = new ConvertEntitytoDTO();
        return convertEntitytoDTO.convert(productsEntity);
    }

    @Override
    public List<ProductsResponse> getAllProduct()
    {
        List<ProductsEntity> productsEntity = productsRepository.findAll();
        ConvertEntitytoDTO convertEntitytoDTO = new ConvertEntitytoDTO();
        return convertEntitytoDTO.convertAll(productsEntity);
    }

    @Override
    public void deleteProduct(Integer id)
    {
        productsRepository.deleteById(id);
    }

    @Override
    public void saveProduct(ProductsRequest productsRequest)
    {
        ConvertDTOtoEntity convertDTOtoEntity = new ConvertDTOtoEntity();
         productsRepository.save(convertDTOtoEntity.convert(productsRequest));
    }
    @Override
    public void updateProduct(Integer id, ProductsRequest productsRequest)
    {
        ProductsEntity productsEntity = productsRepository.findById(id)
                .orElseThrow(() -> new ResourceAccessException("Resource Not Found" + id));
        ConvertDTOtoEntity convertDTOtoEntity = new ConvertDTOtoEntity();
        ProductsEntity returnedValue = convertDTOtoEntity.convert(productsRequest);
        productsEntity.setName(returnedValue.getName());
        productsEntity.setPrice(returnedValue.getPrice());
        productsRepository.save(productsEntity);
    }
}
