package com.example.demo.Convert;

import com.example.demo.Model.ProductsEntity;
import com.example.demo.dto.ProductsResponse;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class ConvertEntitytoDTO {
    public ProductsResponse convert(ProductsEntity productsEntity) {
        ProductsResponse productsResponse = new ProductsResponse();
        productsResponse.setName(productsEntity.getName());
        productsResponse.setPrice(productsEntity.getPrice());
        return productsResponse;
    }

    public List<ProductsResponse> convertAll(List<ProductsEntity> productsEntity) {
        ProductsResponse productsResponse = new ProductsResponse();
        List<ProductsResponse> productsResponseList = new ArrayList<ProductsResponse>();
        productsEntity.forEach(entries -> {
            productsResponse.setName(entries.getName());
            productsResponse.setPrice(entries.getPrice());
            productsResponseList.add(productsResponse);
        });

        return productsResponseList;
    }
}