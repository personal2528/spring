package com.example.demo.Convert;

import com.example.demo.Model.ProductsEntity;
import com.example.demo.dto.ProductsRequest;

public class ConvertDTOtoEntity {

    public ProductsEntity convert(ProductsRequest productsRequest)
    {
        ProductsEntity productsEntity = new ProductsEntity();
        productsEntity.setName(productsRequest.getName());
        productsEntity.setPrice(productsRequest.getPrice());
        return productsEntity;
    }
}
