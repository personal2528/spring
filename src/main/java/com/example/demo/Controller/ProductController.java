package com.example.demo.Controller;

import com.example.demo.Service.ProductService;
import com.example.demo.dto.ProductsRequest;
import com.example.demo.dto.ProductsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping("/products")
    public void save(@RequestBody ProductsRequest productsRequest)
    {
        productService.saveProduct(productsRequest);
    }
    @GetMapping("/products/{id}")
    public void retrieve(@PathVariable("id") Integer id) {
        productService.getProduct(id);
    }

    @GetMapping("/products")
    public List<ProductsResponse> retrieveAll() {
        return productService.getAllProduct();
    }
    @DeleteMapping("/products/{id}")
    public void delete(@PathVariable("id") Integer id) {
        productService.deleteProduct(id);
    }

    @PutMapping("/products/{id}")
    public void update(@PathVariable Integer id, @RequestBody ProductsRequest productsRequest)
    {
        productService.updateProduct(id, productsRequest);
    }
}

