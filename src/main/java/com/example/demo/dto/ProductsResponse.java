package com.example.demo.dto;

import com.example.demo.Model.ProductsEntity;
import lombok.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class ProductsResponse {
    private String name;
    private float price;
}
